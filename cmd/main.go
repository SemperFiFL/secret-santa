package main

import (
	"encoding/base64"
	"fmt"
	"math/rand"
	"os"
)

func main() {
	results := map[string]string{}
	pool := []string{}
	for i, arg := range os.Args {
		if i == 0 || arg == "encode" {
			continue
		}
		results[arg] = ""
		pool = append(pool, arg)
	}
	//This gets stuck on a singleton list but ¯\_(ツ)_/¯
	for giver := range results {
		drawn := giver
		var drawnIndex int
		for drawn == giver {
			drawnIndex = rand.Intn(len(pool))
			drawn = pool[drawnIndex]
		}
		results[giver] = drawn
		pool[drawnIndex] = pool[len(pool)-1]
		pool = pool[:len(pool)-1]
	}
	for giver, receiver := range results {
		if os.Args[len(os.Args)-1] == "encode" {
			receiver = base64.StdEncoding.EncodeToString([]byte(receiver))
		}
		fmt.Printf("%s gives to %s\n", giver, receiver)
	}
}
