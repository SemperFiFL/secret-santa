# Secret Santa

Quick script to match a pool of people for Secret Santa

### Usage

Run the script and pass a list of names with and optional "encode" at the end to obfuscate the choices

`go run cmd/main.go ...string [encode]`

The picking loop is probably getting stuck on a singleton list but I don't care enough to fix it now.
